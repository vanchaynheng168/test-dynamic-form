// import logo from './logo.svg';
import "./App.css";
import Menu from "./components/NavBar";
import { Container, Button } from "react-bootstrap";
import Item from "./components/Question";
import React, { Component } from "react";
import Section from "./components/Section";
import Question from "./components/Question";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      questions: [
        {
          questionID: 1,
          value: "section",
        },
      ],
      // questions: [
      //   {
      //     questionID: 1,
      //     value: "question",
      //   },
      // ],
      // quiz: {
      //   quizID: 1,
      //   title: "Online Exam July.",
      //   sections: [
      //     {
      //       sectionID: 1,
      //       section : "Section 1",
      //       questionType: "Choose the correct answer.",
      //       questions: [
      //         {
      //           questionID: 1,
      //           question : "What is React JS ?",
      //         },
      //       ],
      //     },
      //   ],
      // },
      //Quiz

      quiz: {
        quizID: 1,
        sections: [
          {
            sectionID: 1,
            value: "section",
            questions: [
              {
                questionID: 1,
                value: "question",
              },
            ],
          },
        ],
      },
    };
  }
  onAddQuestion = (id) => {
    let stateQuestions = this.state.quiz.sections.map(
      (section) => section.questions
    );
    console.log("stateQuestionsFirst: ", ...stateQuestions);

    const newId = Math.random().toString(36).substring(7);

    const newCard = [
      {
        questionID: newId,
        value: "question",
      },
    ];

    const testSection = [...this.state.questions, ...newCard];
    console.log("testQuestion: ", testSection);
    const testQuestion = [...stateQuestions, ...newCard];
    console.log(testQuestion);

    const questions = [{ stateQuestions }, newCard];
    console.log("questions: ", questions);
  };

  onAddSection = (id) => {
    let tmpQuestions = addNewCard(id, "4", this.state.questions);
    this.setState({
      questions: tmpQuestions,
    });
  };

  onDelete = (id) => {
    const questions = this.state.questions.filter((ques) => ques.id !== id);
    this.setState({ questions });
  };

  render() {
    console.log("State Qquiz: ", this.state.quiz);
    return (
      <div>
        <Menu />
        <Container>
          {this.state.quiz.sections.map(
            (section) => {
              return section.value === "section" ? (
                <Section
                  key={section.sectionID}
                  id={section.sectionID}
                  onAddQuestion={this.onAddQuestion}
                  onAddSection={this.onAddSection}
                  onDelete={this.onDelete}
                  showSection={this.state.showSection}
                />
              ) : (
                // section.questions.map((question) => (
                //   <Item
                //     key={question.questionID}
                //     id={question.questionID}
                //     onAddQuestion={this.onAddQuestion}
                //     onAddSection={this.onAddSection}
                //     onDelete={this.onDelete}
                //   />
                // ))
                section.questions.map((question) => console.log(question))
              );
            }

            // return section.value === "question" ? (
            //   <Item
            //     key={ques.questionID}
            //     id={ques.questionID}
            //     onAddQuestion={this.onAddQuestion}
            //     onAddSection={this.onAddSection}
            //     onDelete={this.onDelete}
            //   />
            // ) : (
            //   <Section
            //     key={ques.questionID}
            //     id={ques.questionID}
            //     onAddQuestion={this.onAddQuestion}
            //     onAddSection={this.onAddSection}
            //     onDelete={this.onDelete}
            //     showSection={this.state.showSection}
            //   />
            // );
          )}
          <Button className="my-2">Save</Button>
        </Container>
      </div>
    );
  }
}
//Render New Card
const addNewCard = (id, value, questions) => {
  const newId = Math.random().toString(36).substring(7);
  const newCard = {
    // questionID: newId,
    value: value,
  };
  let index = 0;
  for (let i = 0; i < questions.length; i++) {
    if (questions[i].questionID === id) {
      index = i + 1;
      break;
    }
  }
  // console.log("index: ", index);

  questions.splice(index, 0, newCard);
  return questions;
};

// const addNewCard = (id, value, questions) => {
//   const newId = Math.random().toString(36).substring(7);
//   const newCard = {
//     id: newId,
//     value: value,
//   };
//   let index = 0;
//   for (let i = 0; i < questions.length; i++) {
//     if (questions[i].id === id) {
//       index = i + 1;
//       break;
//     }
//   }
//   questions.splice(index, 0, newCard);
//   return questions;
// };
