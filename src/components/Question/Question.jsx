import React, { useState } from "react";
import { Button, Card, Form } from "react-bootstrap";
// import { Switch, Route, Link } from "react-router-dom";
import Answer from "../Answer";
import Multi from "../Multi/Multi";
import Dropdown from "react-dropdown";
import "react-dropdown/style.css";
function Question({ onAddQuestion, onAddSection, onDelete, id }) {
  const [compo, setCompo] = useState(<Answer />);

  const options = [
    { value: <Answer />, label: "Question" },
    { value: <Multi />, label: "Multi" },
  ];
  return (
    <Card className="my-2">
      <Card.Header as="h5">Question</Card.Header>
      <Card.Body>
        <Form.Group>
          <Dropdown
            className="my-2"
            options={options}
            onChange={(e) => {
              setCompo(e.value);
            }}
            value={"Question"}
            placeholder="Select an option"
          />
          {compo}
        </Form.Group>
        <div className="text-right">
          <Button
            variant="primary"
            className="mx-2"
            onClick={() => onAddQuestion(id)}>
            Question
          </Button>
          <Button
            variant="warning"
            className="mx-2"
            onClick={() => onAddSection(id)}>
            Section
          </Button>
          <Button
            variant="danger"
            className="mx-2"
            onClick={() => onDelete(id)}>
            Delete
          </Button>
        </div>
      </Card.Body>
    </Card>
  );
}

export default Question;
