import React from "react";
import Item from "../Question";
import { Card, Form } from "react-bootstrap";
function Section({ onAddQuestion, onDelete, id,onAddSection }) {
  return (
    <>
      <div>
        <Card className="my-2">
          <Card.Body>
            <Form.Group>
              <Form.Control
                type="text"
                placeholder="Section..."
                name="Section"
              />
              <Form.Control
                className="my-2"
                type="text"
                placeholder="Instruction..."
                name="Section"
              />
            </Form.Group>
          </Card.Body>
        </Card>
        <Item onAddQuestion={onAddQuestion} onDelete={onDelete} onAddSection={onAddSection} id={id} />
      </div>
    </>
  );
}

export default Section;
