import React from "react";
import { Form } from "react-bootstrap";
function Answer({ onAnswerChange }) {
  return (
    <div>
      {" "}
      <br />
      <Form.Group>
        <h3>What is HTML?</h3>
        <Form.Group controlId="exampleForm.ControlTextarea1">
          <Form.Label>Answer</Form.Label>
          <Form.Control as="textarea" rows="3" />
        </Form.Group>
      </Form.Group>
    </div>
  );
}

export default Answer;
